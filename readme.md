#Paul Pebble

##Beschreibung

Meine erste Watchface für die [Pebble](https://getpebble.com/). Die Watchface lädt die Ergebnisse des letzten [St. Pauli](http://www.fcstpauli.com) Spiels der über die Schnittstelle der [openLigaDB](http://www.openligadb.de/) unter Verwendung dieser [JSON API](https://openligadb-json.heroku.com/). Im Moment werden nur die Spiele der 2. Bundesliga unterstützt.

Realisiert ist die App nicht mit dem SDK, der auf C basiert, sondern mit  [Pebble.js](http://developer.getpebble.com/getting-started/pebble-js-tutorial/part1.html). Pebble.js ist ein Framework für Pebbel OS mit dem sich Applikationen rein mit Javascript realisieren lassen. Im Moment ist das Framework noch Beta und es fehlen noch Funktionen gegenüber dem C SDK.

![Screenshot](http://www.matthias-faller.de/wp-content/uploads/2014/10/pauli_pebble_watch_screenshot.png "Screenshot")

##Anleitung

[Pauli_Pebble.pbw](https://bitbucket.org/matthiasfaller/pauli-pebble/downloads/Pauli_Pebble.pbw) nach dieser [Anleitung](http://www.german-pebblers.de/viewtopic.php?f=3&t=321) installieren. Als erste Gehversuche und wegen des Betastatus des Frameworks ist die App noch nicht im offiziellen App Site 

Durch Schütteln werden die Spieldaten neu geladen.