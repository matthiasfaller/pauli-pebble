var UI = require('ui');
var Vector2 = require('vector2');
var Accel = require('ui/accel');

Accel.init();

var main = new UI.Window({
    fullscreen: true
});

// Elements
// ********

var time_text = new UI.TimeText({
    position: new Vector2(0, 0), 
    size: new Vector2(144, 36),
    backgroundColor: 'black',
    color: 'white',
    font: 'BITHAM_30_BLACK',
    textAlign: 'center',
    text: '%H:%M'
});

var loading_text = new UI.Text({
    position: new Vector2(0, 36), 
    size: new Vector2(144, 132),
    backgroundColor: 'white',
    color: 'black',
    textAlign: 'center',
    font: 'GOTHIC_28',
    textOverflow:'wrap',
    text: 'Loading...'    
});


// Helper
// ******

var format_date_str = function(date_str) {
    //Returns formated date string based on json date string
    
    var date = new Date(date_str);
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear().toString().substr(2,2);
    var hour = date.getHours();
    var minutes = date.getMinutes();
    
    return day + '.' + month + '.' + year + ' ' + hour + ':' + minutes;    
};

var draw_results = function(data) {
    //Draws formates results on elements based in data json object
    
    var end_result = data.match_results.match_result[0];
        
    var match_str = data.name_team1 + '\n' + data.name_team2;
    var result_str = end_result.points_team1 + ':' + end_result.points_team2; 
    var status_str = 'Stand: ' + format_date_str(data.last_update);
    
    var match_text = new UI.Text({
        position: new Vector2(0, 36), 
        size: new Vector2(144, 64),
        backgroundColor: 'white',
        color: 'black',
        textAlign: 'center',
        text: match_str
    });
    
    var result_text = new UI.Text({
        position: new Vector2(0, 100), 
        size: new Vector2(144, 50),
        backgroundColor: 'white',
        color: 'black',
        textAlign: 'center',
        font: 'GOTHIC_28',
        text: result_str
    });
        
    var status_text = new UI.Text({
        position: new Vector2(0, 150), 
        size: new Vector2(144, 18),
        backgroundColor: 'white',
        color: 'black',
        textAlign: 'center',
        font: 'GOTHIC_14',
        text: status_str
    });
        
    main.remove(loading_text);
    main.add(match_text);
    main.add(result_text); 
    main.add(status_text);
};


// Handler
// *******

var load_results = function() {
    // Load results form obenligedb. team_id of FC St. Pauli in current seaion is 98. League_id of current
    // saesion of 2. Bundesliste is 721.
    // https://openligadb-json.heroku.com/ offers a json API for http://www.openligadb.de/
    
    var ajax = require('ajax');
    
    ajax(
        {
            url: 'https://openligadb-json.heroku.com/api/last_match_by_league_team?team_id=98&league_id=721',
            type: 'json'
        },
        function(data) {
            draw_results(data);      
        },
        function(error) {
            console.log('The ajax request failed: ' + error);
            loading_text.text = "Cannot load data, check internet connection";
        }
    );    
};


// Event Listener
// **************

Accel.on('tap', function(e) {    
   load_results();
});

main.add(time_text);
main.add(loading_text);
main.show();

load_results();